import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class ServerMain {
	private final static int PORT = 3000;
	public static List<TaskThread> connections;
	
	public static void main(String[] args) throws IOException {
		connections = new ArrayList<TaskThread>();
		
		ServerSocket server = new ServerSocket(PORT);
		System.out.println("BlackChat Server is running on port: " + PORT); 
        
	    while(true) {                 
	    	Socket socket = server.accept();
            System.out.println("Connected: " + socket);
            TaskThread service = new TaskThread(socket);
            connections.add(service);
            service.start();
	    }
	}
}