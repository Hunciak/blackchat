import java.io.*;
import java.net.*;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TaskThread extends Thread {
	Socket socket;
	
	//ObjectOutputStream outputStream;
    //ObjectInputStream inputStream;
	BufferedReader in;
	BufferedWriter out;
		   
	TaskThread(Socket clientSocket)  {                                   
		this.socket = clientSocket;
		
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}                                   
		   
	public void run() {	
		while(!socket.isClosed()) {                                          
			try {
				String inputLine = null;
				while ((inputLine = in.readLine()) != null) {
			    	ObjectMapper objectMapper = new ObjectMapper();
			    	ChatMessage clientMessage = objectMapper.readValue(inputLine, ChatMessage.class);
			    	System.out.println(clientMessage.getSender() + ": " + clientMessage.getMessage());
			    	sendToEveryone(inputLine);
				}
			} catch (IOException e) {
				try {
					socket.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}                                                    
		}
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void sendMessage(String message) {
		try {
			out.write(message + "\n");
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	private void sendToEveryone(String message) {
        for (TaskThread connection : ServerMain.connections) {
            connection.sendMessage(message);
        }
    }
}
