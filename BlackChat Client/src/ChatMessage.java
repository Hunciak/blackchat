import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChatMessage implements Serializable {

	private static final long serialVersionUID = 5915329194076126916L;
	private String message;
	private String sender;
	
	@JsonCreator
	ChatMessage(@JsonProperty("message") String message,@JsonProperty("sender") String sender) {
		setMessage(message);
		setSender(sender);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
}
