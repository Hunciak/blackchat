import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.fasterxml.jackson.databind.ObjectMapper;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class ClientMain extends Application implements Runnable  {
	private static final int PORT = 3000;
	private static final String HOST = "127.0.0.1";
	private Scene scene;
	private Socket socket;
	
	BufferedReader in;
	BufferedWriter out;
	
	TextField textMsg;
	TextArea textLog;
	Circle circleStatus;
	Label labelStatus;
	Label labelPplOnline;
	
	Thread UIThread;
	
	String nickname = "Anonymous";
	
	private enum AppStatus {
		CONNECTING, ONLINE, OFFLINE
	}
	
	private void closeApp() {
		closeSocket();
		UIThread.stop();
        Platform.exit();
        System.exit(0);
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("Scene.fxml"));
		    
	        scene = new Scene(root, 400, 460);
	    
	        primaryStage.setTitle("BlackChat Client");
	        primaryStage.setScene(scene);
	        primaryStage.setResizable(false);
	        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	            @Override
	            public void handle(WindowEvent t) {
	            	closeApp();
	            }
	        });
	        
	        circleStatus = (Circle) scene.lookup("#circleStatus");
	        labelStatus = (Label) scene.lookup("#labelStatus");
	        labelPplOnline = (Label) scene.lookup("#labelPplOnline");
	        
	        switchAppStatus(AppStatus.CONNECTING);
	        
	        textMsg = (TextField) scene.lookup("#textMsg");
	        textMsg.setOnKeyPressed(event -> {
	        	   if(event.getCode() == KeyCode.ENTER){
	        		   if (textMsg.getText().compareTo("") != 0)
	        				sendMessage();
	        	   }
	        	}); 
	        textLog = (TextArea) scene.lookup("#textLog");
	        textLog.clear();
	        
	        MenuBar menuBar = new MenuBar();
	        Menu file = new Menu("File");
	        Menu edit = new Menu("Edit");
	        Menu help = new Menu("Help");
	        
	        menuBar.getMenus().addAll(file, edit, help);
	        
	        MenuItem quit = new Menu("Quit");
	        file.getItems().addAll(quit);
	        quit.setOnAction(new EventHandler<ActionEvent>() {
	            @Override public void handle(ActionEvent e) {
	            	closeApp();
	            }
	        });
	        
	        MenuItem changeNick = new Menu("Change Nick");
	        edit.getItems().addAll(changeNick);
	        changeNick.setOnAction(new EventHandler<ActionEvent>() {
	            @Override public void handle(ActionEvent e) {
	            	nickname = "LEL";
	            }
	        });
	        
	        MenuItem about = new Menu("About");
	        help.getItems().addAll(about);
	        about.setOnAction(new EventHandler<ActionEvent>() {
	            @Override public void handle(ActionEvent e) {
	            	printLog(">   ( . ) ( . )");
	            }
	        });
	        
	        AnchorPane anchorPane = (AnchorPane) scene.lookup("#anchorPane");
	        
	        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
	        anchorPane.getChildren().add(menuBar);
	        
	        UIThread = new Thread(this);
	        UIThread.start();
			
	        primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void run() {
		connect();
	}
	
	private void switchAppStatus(AppStatus as) {
		switch (as) {
			case CONNECTING:
				labelStatus.setText("Status: Connecting...");
				circleStatus.setFill(Color.BLUE);
				break;
			case ONLINE:
				labelStatus.setText("Status: ONLINE");
				circleStatus.setFill(Color.GREEN);
				break;
			case OFFLINE:
				labelStatus.setText("Status: OFFLINE");
				circleStatus.setFill(Color.RED);
				break;
			default:
				break;
		}
	}
	
	public void sendMessage() {                                                                        
		try {
			ChatMessage msg = new ChatMessage(textMsg.getText(), nickname);
			ObjectMapper objectMapper = new ObjectMapper();
			String value = objectMapper.writeValueAsString(msg);
			out.write(value + "\n");
			out.flush();
		    textMsg.clear();
		} catch (IOException e) {
			printLog("> Can't send message!");
			e.printStackTrace();
		} 
	}
	
	public void connect() {     
		try {
            socket = new Socket(HOST, PORT);
            printLog("> Connected successfully to BlackChat Server!");
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            switchAppStatus(AppStatus.ONLINE);
        } catch (IOException e) {
        	printLog("> Can't connect to server!");
        	switchAppStatus(AppStatus.OFFLINE);
            //e.printStackTrace();
        }

		new Thread() {
            @Override
            public void run() {
            	receiveData();
            }
        }.start();                                                    
	}
	
	private void receiveData() {
        try {
	    	while (!socket.isClosed()) {
	    		String inputLine = null;
	    		while ((inputLine = in.readLine()) != null) {
	    			ObjectMapper objectMapper = new ObjectMapper();
				    ChatMessage clientMessage = objectMapper.readValue(inputLine, ChatMessage.class);
				    printLog(clientMessage.getSender() + ": " + clientMessage.getMessage());
	    		}
        	}
        } catch (IOException e) {
        	printLog("> Error in receiving data!");
            e.printStackTrace();
        }
    }
	
	public void closeSocket() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
	
	private void printLog(String msg) {
		if (textLog.getText().compareTo("") != 0) textLog.appendText("\n" + msg);
		else textLog.appendText(msg);
	}
}
